FROM node:lts
LABEL maintainer="marcos@huck.com.ar"
WORKDIR /usr/app
RUN node --version
RUN npm install -g yarn
COPY package.json .
COPY yarn.lock .
RUN yarn install
COPY . .
RUN yarn prestart:prod
EXPOSE 3000
CMD ["yarn", "start:prod"]