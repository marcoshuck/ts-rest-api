import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { DatabaseModule } from './db.module';

@Module({
  imports: [
    DatabaseModule.forRoot(),
    AuthModule,
    UserModule,
  ],
})
export class AppModule {}
