export interface CrudService<T> {
    findOne(id: number): Promise<T>;
    findAll(): Promise<T[]>;
    delete(id: number): Promise<T>;
    create(entity: any): Promise<T>;
    update(id: number, entity: T): Promise<T>;
}
