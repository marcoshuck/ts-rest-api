export interface CrudController<T> {
    create(entity: any): Promise<T>;
    readAll(): Promise<T[]>;
    read(id: number): Promise<T>;
    update(id: number, entity: T): Promise<T>;
    delete(id: number): Promise<T>;
}
