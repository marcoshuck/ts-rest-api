import { CrudController } from '../interfaces/crud.controller';
import { User } from './user.entity';
import { Controller, Post, Body, Get, Param, Put, Delete, UseGuards, Logger } from '@nestjs/common';
import { UserService } from './user.service';
import { AuthGuard } from '@nestjs/passport';
import { CreateUserDto } from './dtos/create-user.dto';

@Controller('user')
export class UserController implements CrudController<User> {
    private logger: Logger;
    constructor(private readonly userService: UserService) {
        this.logger = new Logger('UserController');
    }

    @UseGuards(AuthGuard('jwt'))
    @Post()
    async create(@Body() entity: CreateUserDto): Promise<User> {
        this.logger.verbose(`Creating new user. Data: ${JSON.stringify(entity)}`);
        return this.userService.create(entity);
    }

    @UseGuards(AuthGuard('jwt'))
    @Get()
    async readAll(): Promise<User[]> {
        this.logger.verbose('Getting all users');
        return this.userService.findAll();
    }

    @UseGuards(AuthGuard('jwt'))
    @Get(':id')
    async read(@Param('id') id: number): Promise<User> {
        this.logger.verbose(`Getting user with id ${id}`);
        return this.userService.findOne(id);
    }

    @UseGuards(AuthGuard('jwt'))
    @Put(':id')
    async update(@Param('id') id: number, @Body() entity: User): Promise<User> {
        this.logger.verbose(`Updating user with id ${id}. Data: ${entity}`);
        return this.userService.update(id, entity);
    }

    @UseGuards(AuthGuard('jwt'))
    @Delete(':id')
    async delete(id: number): Promise<User> {
        this.logger.verbose(`Deleting user with id ${id}`);
        return this.userService.delete(id);
    }
}
