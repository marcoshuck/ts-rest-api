import { Injectable, NotFoundException, Logger, InternalServerErrorException, ForbiddenException } from '@nestjs/common';
import { CrudService } from 'src/interfaces/crud.service';
import { User } from './user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcryptjs';
import { CreateUserDto } from './dtos/create-user.dto';

@Injectable()
export class UserService implements CrudService<User> {
    private logger: Logger;

    constructor(@InjectRepository(User) private readonly userRepository: Repository<User>) {
        this.logger = new Logger('UserService');
    }

    async findOne(id: number): Promise<User> {
        let user: User;

        try {
            user = await this.userRepository.findOne(id);
        } catch (error) {
            this.logger.error(`Failed to find user with id: ${id}`, error.stack);
            throw new InternalServerErrorException();
        }

        if (!user) {
            throw new NotFoundException(`Could not find user with ID ${id}`);
        }

        return user;
    }

    async findAll(): Promise<User[]> {
        return this.userRepository.find();
    }

    async findOneByEmail(email: string): Promise<User> {
        let user: User;
        try {
            user = await this.userRepository.findOne({email});
        } catch (error) {
            this.logger.error(`Failed to find user with email: ${email}`, error.stack);
            throw new InternalServerErrorException();
        }

        if (!user) {
            throw new NotFoundException(`Could not find user with email ${email}`);
        }

        return user;
    }

    async delete(id: number): Promise<User> {
        const user: User = await this.findOne(id);
        try {

            this.userRepository.remove(user);
            delete user.password;
            delete user.salt;
            return user;

        } catch (error) {
            this.logger.error(`Failed to delete user with id: ${id}.`, error.stack);
            throw new InternalServerErrorException();
        }
    }

    async create(entity: CreateUserDto): Promise<User> {
        const found: User = await this.userRepository.findOne({ email: entity.email });
        let user: User = new User();

        if (found) {
            throw new ForbiddenException();
        }

        try {
            user.firstName = entity.firstName;
            user.lastName = entity.lastName;
            user.email = entity.email;
            user.createdAt = new Date();
            user.updatedAt = new Date();
            user.salt = await bcrypt.genSalt();
            user.password = await this.hashPassword(entity.password, user.salt);

            this.logger.debug(`User creation before saving data: ${JSON.stringify(user)}`);

            user = await this.userRepository.save(user);

            this.logger.debug(`User creation after saving data: ${JSON.stringify(user)}`);

            delete user.password;
            delete user.salt;

            return user;
        } catch (error) {
            this.logger.error(`Failed to create new user. Data: ${JSON.stringify(user)}`, error.stack);
            throw new InternalServerErrorException();
        }
    }

    async update(id: number, entity: User): Promise<User> {
        let user = await this.findOne(id);

        const { password, salt } = user;

        const hashedPassword = await this.hashPassword(password, salt);

        if (hashedPassword !== user.password) {
            entity.salt = await bcrypt.genSalt();
            entity.password = await this.hashPassword(entity.password, entity.salt);
        }

        user = entity;

        try {
            this.userRepository.save(user);
        } catch (error) {
            this.logger.error(`Failed to update user with id: ${id}. Data: ${entity}`, error.stack);
            throw new InternalServerErrorException();
        }

        delete user.password;
        delete user.salt;

        return user;
    }

    public async hashPassword(password: string, salt: string): Promise<string> {
        try {
            const hashed: string = await bcrypt.hash(password, salt);
            this.logger.verbose(`Hashing password with text: ${password} and salt: ${salt}. Getting: ${hashed}`);
            return hashed;
        } catch (error) {
            this.logger.error('Failed to hash password', error.stack);
            throw new InternalServerErrorException();
        }
    }

    public async validatePassword(user: User, password: string): Promise<boolean> {
        try {
            this.logger.verbose(`Validating password with user: ${JSON.stringify(user)} and password: ${password}`);
            const hashedPassword: string = await this.hashPassword(password, user.salt);
            this.logger.debug(`Comparing hashed password ${hashedPassword} == ${user.password}`);
            return hashedPassword === user.password;
        } catch (error) {
            this.logger.error('Failed to validate password', error.stack);
            throw new InternalServerErrorException();
        }
    }
}
