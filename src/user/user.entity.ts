import { PrimaryGeneratedColumn, Column, UpdateDateColumn, CreateDateColumn, Entity, BaseEntity } from 'typeorm';

@Entity('user')
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: 64 })
    firstName: string;

    @Column({ type: 'varchar', length: 64 })
    lastName: string;

    @Column({ type: 'varchar', length: 250, nullable: true })
    avatar: string;

    @Column({ type: 'varchar', length: 320, unique: true })
    email: string;

    @Column({ type: 'date', nullable: true })
    birthday: Date;

    @Column()
    password?: string;

    @Column({ nullable: true })
    salt?: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

}
