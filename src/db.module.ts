import { TypeOrmModule } from '@nestjs/typeorm';
import { Module, DynamicModule } from '@nestjs/common';
import { User } from './user/user.entity';

@Module({})
export class DatabaseModule {
    static async forRoot(): Promise<DynamicModule> {
        await new Promise((resolve) => setTimeout(resolve, 1000));
        return {
            module: DatabaseModule,
            imports: [
                TypeOrmModule.forRoot({
                    type: 'postgres',
                    host: process.env.DB_HOST,
                    port: parseInt(process.env.DB_PORT, 10),
                    username: process.env.POSTGRES_USER,
                    password: process.env.POSTGRES_PASSWORD,
                    database: process.env.POSTGRES_DB,
                    entities: [User],
                    migrations: [],
                    synchronize: true,
                    keepConnectionAlive: true,
                    retryAttempts: 2,
                    retryDelay: 1000,
                    logging: true,
                }),
            ],
        };
    }
}
