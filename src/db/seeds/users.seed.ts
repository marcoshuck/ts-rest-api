import { User } from '../../user/user.entity';
import * as bcrypt from 'bcryptjs';

const salt: string = bcrypt.genSaltSync();
const password: string = bcrypt.hashSync('demo', salt);

let user: User = new User();
user = {
  id: 1,
  firstName: 'Demo',
  lastName: 'Huck',
  email: 'demo@huck.com.ar',
  password,
  salt,
  birthday: new Date(),
  // tslint:disable-next-line: max-line-length
  avatar: 'https://avataaars.io/?avatarStyle=Circle&topType=ShortHairShortWaved&accessoriesType=Blank&hairColor=Black&facialHairType=Blank&clotheType=BlazerSweater&eyeType=Default&eyebrowType=Default&mouthType=Smile&skinColor=Light',
  createdAt: new Date(),
  updatedAt: new Date(),
};

export const UsersSeed: User[] = [
  user,
];
