import { Injectable, UnauthorizedException, Logger, InternalServerErrorException, BadRequestException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtResponse } from './jwt.response';
import { JwtPayload } from './jwt.payload';
import { JwtService } from '@nestjs/jwt';
import { User } from '../user/user.entity';
import { AuthCredentialDto } from './dtos/auth-credential.dto';

@Injectable()
export class AuthService {
    private logger: Logger;
    constructor(
        private readonly userService: UserService,
        private readonly jwtService: JwtService,
    ) {
        this.logger = new Logger('AuthService');
    }

    async login(credential: AuthCredentialDto): Promise<JwtResponse> {
        let email: string;
        let password: string;
        try {
            email = credential.email;
            password = credential.password;
        } catch (error) {
            throw new BadRequestException();
        }

        const user: User = await this.userService.findOneByEmail(email);

        const isValidPassword: boolean = await this.userService.validatePassword(user, password);

        if (!isValidPassword) {
            throw new UnauthorizedException('Invalid credentials');
        }

        const userId: number = user.id;

        delete user.id;
        delete user.password;
        delete user.salt;

        try {
            const accessToken: string = await this.jwtService.sign({user});

            const response: JwtResponse = {
                accessToken,
                expiresIn: Number(process.env.EXPIRATION_TIME),
                userId,
            };

            return response;
        } catch (error) {
            this.logger.error(`Failed to create user token for signing in with id: ${userId}`, error.stack);
            throw new InternalServerErrorException();
        }
    }
}
