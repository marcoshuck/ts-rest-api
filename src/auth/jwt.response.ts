export class JwtResponse {
    expiresIn: number;
    accessToken: string;
    userId?: number;
}
