import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { User } from '../user/user.entity';
import { JwtPayload } from './jwt.payload';
import { UserService } from '../user/user.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly userService: UserService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.SECRET_KEY || '1v3ryd1ff1cult',
        });
    }

    async validate(payload: JwtPayload) {
        const user: User = await this.userService.findOneByEmail(payload.user.email);

        if (!user) {
            throw new UnauthorizedException();
        }

        return user;
    }
}
