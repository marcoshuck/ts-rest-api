import { User } from '../user/user.entity';

export class JwtPayload {
    user: User;
    iat: number;
    exp: number;
}
