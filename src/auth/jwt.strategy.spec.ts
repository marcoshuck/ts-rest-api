import { JwtStrategy } from './jwt.strategy';
import { Test } from '@nestjs/testing';
import { UserService } from '../user/user.service';
import { User } from '../user/user.entity';
import { JwtPayload } from './jwt.payload';
import { UnauthorizedException } from '@nestjs/common';

const mockUserService = () => ({
  findOneByEmail: jest.fn(),
});

describe('JwtStrategy', () => {
  let jwtStrategy: JwtStrategy;
  let userService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        JwtStrategy,
        { provide: UserService, useFactory: mockUserService },
      ],
    }).compile();

    jwtStrategy = await module.get<JwtStrategy>(JwtStrategy);
    userService = await module.get<UserService>(UserService);
  }),

  describe('token validation', () => {
    it('should return a user based on JWT payload after validation', async () => {
      const user = new User();
      user.firstName = 'Demo';
      user.lastName = 'User';
      user.email = 'demo@huck.com.ar';
      user.id = 1;

      const payload: JwtPayload = {
        user,
        iat: 0,
        exp: 0,
      };

      expect(userService.findOneByEmail).not.toHaveBeenCalled();
      userService.findOneByEmail.mockResolvedValue(user);
      const result = await jwtStrategy.validate(payload);
      expect(userService.findOneByEmail).toHaveBeenCalledWith(payload.user.email);
      expect(result).toEqual(user);
    });

    it('should throw an unauthorized exception when user is not found', () => {
      const user = new User();
      user.firstName = 'Demo';
      user.lastName = 'User';
      user.email = 'demo@huck.com.ar';
      user.id = 1;

      const payload: JwtPayload = {
        user,
        iat: 0,
        exp: 0,
      };

      userService.findOneByEmail.mockResolvedValue(null);
      expect(jwtStrategy.validate(payload)).rejects.toThrow(UnauthorizedException);
    });
  });
});
