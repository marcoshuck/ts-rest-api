import { Controller, Body, Post, Logger } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthCredentialDto } from './dtos/auth-credential.dto';
import { JwtResponse } from './jwt.response';

@Controller('auth')
export class AuthController {
  private logger: Logger;
  constructor(private readonly authService: AuthService) {
    this.logger = new Logger('AuthController');
  }

  @Post('/user/signup')
  async registerUser(@Body() credential: AuthCredentialDto): Promise<any> {
    return await this.authService.login(credential);
  }

  @Post('/user/login')
  async loginUser(@Body() credential: AuthCredentialDto): Promise<JwtResponse> {
    this.logger.verbose('User signing in. Credentials: ' + JSON.stringify(credential));
    return await this.authService.login(credential);
  }
}
