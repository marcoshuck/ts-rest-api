import { Test } from '@nestjs/testing';
import { JwtPayload } from './jwt.payload';
import { AuthService } from './auth.service';
import { AuthCredentialDto } from './dtos/auth-credential.dto';
import { JwtResponse } from './jwt.response';
import { AuthController } from './auth.controller';
import { UnauthorizedException, InternalServerErrorException, BadRequestException } from '@nestjs/common';

const mockAuthService = () => ({
  login: jest.fn(),
});

describe('AuthController', () => {
  let authController: AuthController;
  let authService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        AuthController,
        { provide: AuthService, useFactory: mockAuthService },
      ],
    }).compile();

    authController = await module.get<AuthController>(AuthController);
    authService = await module.get<AuthService>(AuthService);
  });

  describe('Login', () => {
    it('should return a JWT Response using the credential received in body', async () => {
      const credential: AuthCredentialDto = {
        email: 'demo@huck.com.ar',
        password: 'example1234',
      };
      const response: JwtResponse = {
        accessToken: 'exampleToken',
        expiresIn: 0,
      };

      expect(authService.login).not.toHaveBeenCalled();
      authService.login.mockResolvedValue(response);
      const result = await authController.loginUser(credential);
      expect(authService.login).toHaveBeenCalled();
      expect(result).toEqual(response);
    });

    it('should return a bad request exception when using an undefined credential', async () => {
      expect(authService.login).not.toHaveBeenCalled();
      authService.login.mockRejectedValue(new BadRequestException());
      expect(authController.loginUser(null)).rejects.toThrow(BadRequestException);
      expect(authService.login).toHaveBeenCalled();
    });
  });
});
